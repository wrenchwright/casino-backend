<?php

// config for Wainwright/CasinoDogOperatorApi
return [

    'access' => [
        'key' => 'd658d5de0ae5b88db9c9dc657ebb0236',
        'secret' => 'oYLAx2Y5BhxP',
    ],

    'test_settings' => [
        'start_balance' => 10000, // enter starting balance (integer in cents)
    ],

    /* Firewall is used within RestrictIpAddressMiddleware */
    'firewall' => [
        'https_only' => false, // redirect requests in http to https
        'restrict_callback' => false,
        'restrict_all_routes' => true, // restrict full app on ip
        'allowed_ip' => [
          '85.148.48.255',
          '127.0.0.1'
        ],
      ],

    'endpoints' => [
        'create_session' => 'https://win.radio.fm/api/createSession',
        'available_games' => 'https://win.radio.fm/api/availableGames'
    ],
];
