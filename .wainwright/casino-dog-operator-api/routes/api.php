<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Wainwright\CasinoDogOperatorApi\Controllers\CasinoDogCallbackController;
use Wainwright\CasinoDogOperatorApi\Controllers\CasinoDogCreateSessionController;

Route::middleware('api', 'throttle:5000,1')->prefix('api/casino-dog-operator-api/')->group(function () {
        Route::get('/callback', [CasinoDogCallbackController::class, 'callback']);
        Route::get('/createSession', [CasinoDogCreateSessionController::class, 'test_create']);
});

