<?php

namespace Wainwright\CasinoDogOperatorApi;

use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Wainwright\CasinoDogOperatorApi\Commands\CasinoDogOperatorApiCommand;

class CasinoDogOperatorApiServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        $package
            ->name('casino-dog-operator-api')
            ->hasConfigFile()
            ->hasViews()
            ->hasRoutes(['api'])
            ->hasMigration('create_playerbalances_table')
            ->hasCommand(CasinoDogOperatorApiCommand::class);
                        
            $kernel = app(\Illuminate\Contracts\Http\Kernel::class);
            $kernel->pushMiddleware(\Wainwright\CasinoDogOperatorApi\Middleware\RestrictIpAddressMiddleware::class);

    }
}
