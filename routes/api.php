<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::middleware('api')->any('/games', function (Request $request) {
    $page = $request->page ? $request->page : 1;
    $limit = $request->limit ? $request->limit : 25;
    $cache = Cache::get($request->fullUrl());
    if(!$cache) {
        $http = Http::get('https://win.radio.fm/api/gameslist/wainwright_casino?page='.$page.'&limit='.$limit);
        $cache = Cache::set($request->fullUrl(), $cache, 500);
    } else{
        $http = $cache;
    }
    if($request->search) {
        $provider_explode = explode('categories.slug:', $request->search);
        if(isset($provider_explode[1])) {
            $explode = explode(';', $provider_explode[1]);
            $provider = $explode[0];
            $http = Http::get('https://win.radio.fm/api/gameslist/wainwright_casino?page='.$page.'&limit='.$limit.'&provider='.$provider);
        }
    }

    return $http;
});

Route::middleware('api')->any('/games/{slug}', function ($slug, Request $request) {
    $http = Http::get('https://win.radio.fm/api/gameinfo/'.$slug);
    if($http->status() !== 200) {
        abort(400, 'Error trying to create session');
        return [];
    }
    $http = json_decode($http, true);

    /* Generate a user ID based on IP that resets every hour */
    $ip = $request->ip();
    $format_time_to_hour = Carbon\Carbon::parse(now())->format('H');
    $player_id = md5($ip.$format_time_to_hour);


    $create_session = Http::get("https://win.radio.fm/api/createSession?game=".$http['slug']."&player=".$player_id."&currency=USD&operator_key=d658d5de0ae5b88db9c9dc657ebb0236&mode=real");
    $create_session = json_decode($create_session, true);

    $orig_tags = $http['tags'];
    $tags = [];
    if(str_contains(json_encode($orig_tags), 'slots')) {
        $data = [
            'id' => 1,
            'name' => 'Slots',
            'slug' => 'slots',
            'icon' => null,
            'image' => [],
            'details' => 'Slotmachine Games',
            'type_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => null,
            'type' => null,
        ];
        array_push($tags, $data);
    }
    if(str_contains(json_encode($orig_tags), 'live')) {
        $data = [
            'id' => 2,
            'name' => 'Live',
            'slug' => 'live',
            'icon' => null,
            'image' => [],
            'details' => 'Live Games',
            'type_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => null,
            'type' => null,
        ];
        array_push($tags, $data);
    }

    if(str_contains(json_encode($orig_tags), 'bonusbuy')) {
        $data = [
            'id' => 3,
            'name' => 'Bonus Buy',
            'slug' => 'bonusbuy',
            'icon' => null,
            'image' => [],
            'details' => 'Bonus Buy Feature',
            'type_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => null,
            'type' => null,
        ];
        array_push($tags, $data);
    }

    if(str_contains(json_encode($orig_tags), 'jackpot')) {
        $data = [
            'id' => 4,
            'name' => 'jackpot',
            'slug' => 'Jackpot',
            'icon' => null,
            'image' => [],
            'details' => 'Jackpot',
            'type_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => null,
            'type' => null,
        ];
        array_push($tags, $data);
    }

    if(str_contains(json_encode($orig_tags), 'casino')) {
        $data = [
            'id' => 4,
            'name' => 'Casino Table Game',
            'slug' => 'casino',
            'icon' => null,
            'image' => [],
            'details' => 'Casino Table Game',
            'type_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => null,
            'type' => null,
        ];
        array_push($tags, $data);
    }

    if(str_contains(json_encode($orig_tags), 'live')) {
        $data = [
            'id' => 1,
            'name' => 'Live',
            'slug' => 'live',
            'icon' => null,
            'image' => [],
            'details' => 'Live Games',
            'type_id' => 3,
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => null,
            'type' => null,
        ];
        array_push($tags, $data);
    }

    $data = [
        'id' => $http['id'],
        'name' => $http['name'],
        'slug' => $http['slug'],
        'player_id' => $player_id,
        'real_url' => $create_session['message']['session_url'],
        'description' => $http['description'],
        'balance' => "99.99",
        'play_real' =>  $create_session['message']['session_url'],
        'fake_iframe_url' => $create_session['message']['fake_iframe_url'],
        'status' => 'active',
        'image' => $http['image'],
        'tags' =>  $tags,
        'type' => $http['type'],
        'provider' => [
            'name' => $http['provider'],
            'slug' => $http['provider'],
            'cover_image' => [
                'original' => $http['image'],
                'thumbnail' => $http['image'],
            ],
            'logo' => [
                'original' => $http['image'],
                'thumbnail' => $http['image'],
            ],
            'is_active' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ],
    ];


    return $data;

});


Route::middleware('api')->any('/tags ', function (Request $request) {
    $http = Http::get('https://win.radio.fm/api/tags');

    return $http;
});





Route::middleware('api')->any('/categories ', function (Request $request) {
    if($request->page) {
        $http = Http::get('https://win.radio.fm/api/providersListEndpoint?scheme_layout=wainwright_casino&page='.$request->page);
    } else {
    $http = Http::get('https://win.radio.fm/api/providersListEndpoint?scheme_layout=wainwright_casino');
    }
    return $http;
});


